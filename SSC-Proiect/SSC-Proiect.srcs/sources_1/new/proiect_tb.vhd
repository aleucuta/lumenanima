----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/04/2020 02:50:37 PM
-- Design Name: 
-- Module Name: proiect_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity proiect_tb is
--  Port ( );
end proiect_tb;

architecture Behavioral of proiect_tb is
    signal SW : STD_LOGIC_VECTOR (7 downto 0) := x"00";
    signal BTN : STD_LOGIC_VECTOR (4 downto 0) := "00000";
    signal CLK : STD_LOGIC := '0';
    signal LED : STD_LOGIC_VECTOR (15 downto 0);
    signal SSEG_CA : STD_LOGIC_VECTOR (7 downto 0);
    signal SSEG_AN : STD_LOGIC_VECTOR (3 downto 0);
    signal UART_RX : STD_LOGIC := '1';
    signal PMODBT_RST : STD_LOGIC;
    signal PMODBT_CTS : STD_LOGIC;
    
    
    constant CLK_PERIOD: TIME:= 10 ns; --frecventa de 100mhz ca si pe placa
    
    
    
    constant c_CLKS_PER_BIT : integer := 10416;
 
 
 
    constant c_BIT_PERIOD : time := 104166 ns; --timp asteptat intre semnale de pe rx
    
    procedure UART_WRITE_BYTE (
        i_data_in       : in  std_logic_vector(7 downto 0);
        signal o_serial : out std_logic) is
    begin
     
        -- Send Start Bit
        o_serial <= '0';
        wait for c_BIT_PERIOD;
     
        -- Send Data Byte
        for ii in 0 to 7 loop
            o_serial <= i_data_in(ii);
            wait for c_BIT_PERIOD;
        end loop;  -- ii
     
        -- Send Stop Bit
        o_serial <= '1';
        wait for c_BIT_PERIOD;
    end UART_WRITE_BYTE;
begin
   
    Clk <= not Clk after CLK_PERIOD/2;
    
    main : entity WORK.main port map(   clk => clk,
                                        sw => sw,
                                        btn => btn,
                                        led => led,
                                        sseg_ca => sseg_ca,
                                        sseg_an => sseg_an,
                                        uart_rx => uart_rx,
                                        pmodbt_rst => pmodbt_rst,
                                        pmodbt_cts => pmodbt_cts);

    process 
        variable data_in : std_logic_vector(7 downto 0);
        variable led_corect: std_logic_vector(15 downto 0);
        variable NrErori : INTEGER:= 0;
    begin
        wait for clk_period;
        
        data_in := x"11"; 
        UART_WRITE_BYTE(data_in, uart_rx);
        wait for clk_period;
        
        data_in := x"DA"; 
        UART_WRITE_BYTE(data_in, uart_rx);
        wait for clk_period;
        
        led_corect := x"DA11";
        
        if (led_corect /= led) then 
            report "Rezultat neasteptat la t = "& TIME'image(now)
            severity ERROR;
            NrErori := NrErori + 1;
        end if;
        
    end process;
end Behavioral;
