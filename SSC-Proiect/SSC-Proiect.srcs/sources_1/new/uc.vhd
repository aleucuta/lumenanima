----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/24/2019 04:26:04 PM
-- Design Name: 
-- Module Name: uc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uc is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           ready : in STD_LOGIC;
           word : in STD_LOGIC_VECTOR (15 downto 0);
           led_state : out STD_LOGIC_VECTOR (15 downto 0));
end uc;

architecture Behavioral of uc is
    type stare_uc is (idle, playing);
    signal stare : stare_uc  := idle;
begin
    process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                stare <= idle;
            else
                case stare is
                    when idle =>    if ready = '1' and  word = x"DA11" then
                                        stare <= playing;
                                    end if;
                    when playing => stare <= playing;
                    when others => stare <= idle;
                end case;
            end if;
        end if;
    end process;
    
    process(stare)
    begin
        case stare is
            when idle =>    led_state <= x"0000";
            when playing => led_state <= word;
            when others => 
        end case;
    end process;
end Behavioral;
