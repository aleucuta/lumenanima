----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/24/2019 05:16:21 PM
-- Design Name: 
-- Module Name: main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
    Port ( 
           BTN : in  STD_LOGIC_VECTOR (4 downto 0);
           CLK : in  STD_LOGIC;
           LED : out  STD_LOGIC_VECTOR (15 downto 0);
--           SSEG_CA : out  STD_LOGIC_VECTOR (7 downto 0);
--           SSEG_AN : out  STD_LOGIC_VECTOR (3 downto 0);
           UART_RX : in  STD_LOGIC --;
--           UART_TXD : out  STD_LOGIC; --debug
--           UART_RXD : in  STD_LOGIC;--debug
--           UART_TX : in  STD_LOGIC; --debug
--           S1 : in  STD_LOGIC;--debug
--           S2 : in  STD_LOGIC;--debug
--           S3 : in  STD_LOGIC;--debug
--           S4 : in  STD_LOGIC;--debug
--           PMODBT_RST : out  STD_LOGIC;
--           PMODBT_CTS : out  STD_LOGIC
               );
end main;

architecture Behavioral of main is
    signal word, led_state: STD_LOGIC_VECTOR(15 downto 0);
    signal data: STD_LOGIC_VECTOR(15 downto 0);
    signal rx_rdy, ready, ud_rst, rst: STD_LOGIC;
    signal clk2: STD_LOGIC := '0';
    signal rx_data : STD_LOGIC_VECTOR(7 downto 0);
    signal cnt : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal aux : STD_LOGIC;
begin
    process(clk)
    begin
        if rising_edge(clk) then
            cnt <= cnt + 1; 
            if  conv_integer(cnt) = 651 then
                cnt <= (others => '0');
                clk2 <= '1';
            else
                clk2 <= '0';
            end if;
        end if;
    end process;
--    UART_TXD <= UART_RX; --debug
    --UART_TX <= UART_RXD;
    rst <= btn(0);
--    debounce : entity WORK.btn_debounce port map (  clk => clk,
--                                                    btn_i => ud_rst,
--                                                    btn_o => rst);
    rx_fsm : entity WORK.rx_fsm port map(   clk => clk2,
                                            rx  => UART_RX,
                                            rst => rst,
                                            rx_rdy => rx_rdy,
                                            rx_data => rx_data);
 
                                            
                                            
    state_reader : entity WORK.state_reader port map (  clk => clk,
                                                        rx_rdy => rx_rdy,
                                                        rx_data => rx_data,
                                                        rdy => ready,
                                                        rst => rst,
                                                        led_state => word);
    uc : entity WORK.uc port map (  clk =>clk,
                                    rst => rst,
                                    word => word,
                                    ready => ready,
                                    led_state => led_state);
    
--    dispL7seg : entity WORK.displ7seg  port map (   clk => clk,  
--                                               rst => rst,
--                                               data => data,  
--                                               an => SSEG_AN,
--                                               seg => SSEG_CA);
    data <= word;
--    PMODBT_RST <= '1';
--    PMODBT_CTS <= '1';                                                  
    led <= led_state(15 downto 0);
end Behavioral;
