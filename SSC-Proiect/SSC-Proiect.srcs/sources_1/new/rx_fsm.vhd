----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/24/2019 01:28:40 PM
-- Design Name: 
-- Module Name: rx_fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rx_fsm is
    Port ( rx : in STD_LOGIC;
           rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           rx_rdy : out STD_LOGIC := '0';
           rx_data : out STD_LOGIC_VECTOR(7 downto 0) := (others => '1'));
end rx_fsm;

architecture Behavioral of rx_fsm is
    type stare_rx_fsm is (wait_s, idle, start, bit_s, stop);
    signal data : STD_LOGIC_VECTOR(7 downto 0) := (others => '1');
    signal stare : stare_rx_fsm := (idle);
    signal baud_cnt : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
    signal bit_cnt : STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                stare <= idle;
            else
                case stare is 
                    when idle =>    if rx = '0' then
                                        stare <= start;
                                        bit_cnt <= (others => '0');
                                        baud_cnt <= (others => '0');
                                    end if;
                    when start =>   baud_cnt <= baud_cnt + 1;
                                    if rx = '1' then
                                        stare <= idle;
                                    else
                                        if conv_integer(baud_cnt) = 6 then
                                            bit_cnt <= (others => '0');
                                            baud_cnt <= (others => '0');
                                            stare <= bit_s;
                                        end if;     
                                    end if;
                    when bit_s =>   baud_cnt <= baud_cnt + 1;
                                    if conv_integer(baud_cnt) = 15 then
                                        data(conv_integer(bit_cnt)) <= rx;
                                        bit_cnt <= bit_cnt + 1;
                                        baud_cnt <= (others => '0');
                                    end if;
                                    
                                    
                                    if conv_integer(bit_cnt) = 7 and conv_integer(baud_cnt) = 15 then
                                        stare <= stop;
                                        bit_cnt <= (others => '0');
                                        baud_cnt <= (others => '0');
                                    end if;   
                                            
                    when stop =>    baud_cnt <= baud_cnt + 1;
                                    if conv_integer(baud_cnt) = 15 then
                                        baud_cnt <= (others => '0');
                                        stare <= wait_s;
                                    end if;
                    when wait_s =>  baud_cnt <= baud_cnt + 1;
                                    if conv_integer(baud_cnt) = 7 then
                                        baud_cnt <= (others => '0');
                                        stare <= idle;
                                    end if;
                    when others =>  stare <= idle;
                end case;
            end if;
        end if;
    end process;
    
    process(stare)
    begin
        case stare is
            when idle   => 
            when start  =>  rx_rdy <= '0';
            when bit_s  =>  
            when stop   =>  
            when wait_s =>  rx_rdy <= '1'; rx_data <= data;
            when others =>
        end case;
    end process;
    
    
end Behavioral;
