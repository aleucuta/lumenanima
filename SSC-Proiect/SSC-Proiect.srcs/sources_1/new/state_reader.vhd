----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/24/2019 03:06:26 PM
-- Design Name: 
-- Module Name: state_reader - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity state_reader is
    Port ( rx_rdy : in STD_LOGIC;
           rst : in STD_LOGIC;
           rx_data : in STD_LOGIC_VECTOR (7 downto 0);
           rdy : out STD_LOGIC := '0';
           led_state : out STD_LOGIC_VECTOR (15 downto 0);
           clk : in STD_LOGIC);
end state_reader;

architecture Behavioral of state_reader is

    signal word : STD_LOGIC_VECTOR (15 downto 0);
    signal current_byte : STD_LOGIC_VECTOR(1 downto 0) := "00";
    signal old_rx_rdy : STD_LOGic := '0';
begin
    process(clk)
    begin 
--    if rst = '1' then
--            word <= (others => '0');
--            current_byte <= "00";
--        else
        if rising_edge(clk) then
           
                if rx_rdy = '1' and rx_rdy /= old_rx_rdy then
                    if conv_integer(current_byte) = 1 then
                        current_byte <= "00";
                        word(15 downto 8) <= rx_data;
                        rdy <= '1';
                    else
                        current_byte <= "01";
                        word(7 downto 0) <= rx_data;
                        rdy <= '0';
                    end if;
                end if;
--            end if;
            
            old_rx_rdy <= rx_rdy;
        end if;
    end process;
    led_state <= word;
end Behavioral;
