package com.lumenanima.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.ArraySet;

import com.lumenanima.model.Animation;
import com.lumenanima.model.AnimationState;

import java.util.ArrayList;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;


public class SQLiteAdapter {
    myDbHelper myhelper;

    public SQLiteAdapter(Context context)
    {
        myhelper = new myDbHelper(context);
    }

    public void deleteAll(){
        SQLiteDatabase dbb = myhelper.getWritableDatabase();
        dbb.delete(myDbHelper.TABLE_ANIMATION_STATE, null,null);
        dbb.delete(myDbHelper.TABLE_ANIMATION,null,null);
    }

    public long insertAnimation(Animation animation)
    {
        SQLiteDatabase dbb = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID_ANIMATION,myDbHelper.NAME,myDbHelper.PERIOD};
        String whereClause = myDbHelper.NAME + " = ?";
        String[] whereArgs = { animation.getName() };
        Cursor cursor = dbb.query(myDbHelper.TABLE_ANIMATION, columns, whereClause,whereArgs,null ,null,null);
        if(cursor.moveToNext() != false){
            return 0;
            //elementul exista deja
        }
        else{
            ContentValues contentValues = new ContentValues();
            contentValues.put(myDbHelper.NAME, animation.getName());
            contentValues.put(myDbHelper.PERIOD, animation.getPeriod());
            long id = dbb.insert(myDbHelper.TABLE_ANIMATION, null , contentValues);
            return id;
        }
    }

    public long insertState(AnimationState animationState, long animationID){
        SQLiteDatabase dbb = myhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(myDbHelper.STATE_PARENT_ID, animationID);
        contentValues.put(myDbHelper.STATE, animationState.getIntState());
        long id = dbb.insert(myDbHelper.TABLE_ANIMATION_STATE, null , contentValues);
        return id;
    }

    public int deleteStanceByPosition(long animationId,int position){
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID_STATE,myDbHelper.STATE_PARENT_ID,myDbHelper.STATE};
        String whereClause = myDbHelper.STATE_PARENT_ID + " = ?";
        String[] whereArgs = { Long.toString(animationId) };
        String[] deleteWhereArgs ={};
        Cursor cursor = db.query(myDbHelper.TABLE_ANIMATION_STATE, columns, whereClause,whereArgs,null ,null,null);
        while(cursor.moveToNext() && position!=0){
            position--;
            if(position == 0){
                long cid = cursor.getLong(cursor.getColumnIndex(myDbHelper.UID_STATE));
                deleteWhereArgs = new String[1];
                deleteWhereArgs[0] = Long.toString(cid);
                db.delete(myDbHelper.TABLE_ANIMATION_STATE,myDbHelper.UID_STATE + " = ?",deleteWhereArgs);
                return 1;
            }
        }
        return 0;
    }

    public AnimationState getState(long id){
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID_STATE,myDbHelper.STATE_PARENT_ID,myDbHelper.STATE};
        String whereClause = myDbHelper.UID_STATE + " = ?";
        String[] whereArgs = { Long.toString(id) };
        Cursor cursor = db.query(myDbHelper.TABLE_ANIMATION_STATE, columns, whereClause,whereArgs,null ,null,null);
        cursor.moveToNext(); //accesam primul element

        int state = cursor.getInt(cursor.getColumnIndex(myDbHelper.STATE));
        return new AnimationState(state);
    }

    public Set<Animation> getAllAnimations(){
        Set<Animation> animations = new ArraySet<Animation>();
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID_ANIMATION,myDbHelper.NAME,myDbHelper.PERIOD};
        String whereClause = "";
        String[] whereArgs = { };
        Cursor cursor = db.query(myDbHelper.TABLE_ANIMATION, columns, null,null,null ,null,null);
        while(cursor.moveToNext()){ //accesam primul element
            String name = cursor.getString(cursor.getColumnIndex(myDbHelper.NAME));
            int period = cursor.getInt(cursor.getColumnIndex(myDbHelper.NAME));
            long cid = cursor.getInt(cursor.getColumnIndex(myDbHelper.UID_ANIMATION));

            Animation newAnimation = new Animation(name,period,getStates(cid));
           // if(animations.contains(newAnimation) == false)
                animations.add(newAnimation);
        }
        return animations;
    }

    public long getAnimationId(String animationName){
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID_ANIMATION,myDbHelper.NAME,myDbHelper.PERIOD};
        String whereClause = myDbHelper.NAME + " = ?";
        String[] whereArgs = { animationName };
        Cursor cursor = db.query(myDbHelper.TABLE_ANIMATION, columns, whereClause,whereArgs,null ,null,null);
        cursor.moveToNext(); //accesam primul element

        return cursor.getInt(cursor.getColumnIndex(myDbHelper.UID_ANIMATION));
    }

    public Animation getAnimation(String animationName){
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID_ANIMATION,myDbHelper.NAME,myDbHelper.PERIOD};
        String whereClause = myDbHelper.NAME + " = ?";
        String[] whereArgs = { animationName };
        Cursor cursor = db.query(myDbHelper.TABLE_ANIMATION, columns, whereClause,whereArgs,null ,null,null);
        cursor.moveToNext(); //accesam primul element

        long cid = cursor.getInt(cursor.getColumnIndex(myDbHelper.UID_ANIMATION));
        int period = cursor.getInt(cursor.getColumnIndex(myDbHelper.PERIOD));
        String name = cursor.getString(cursor.getColumnIndex(myDbHelper.NAME));

        return new Animation(name,period,getStates(cid));
    }
    public ArrayList<AnimationState> getStates(long id){
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {myDbHelper.UID_STATE,myDbHelper.STATE_PARENT_ID,myDbHelper.STATE};
        String whereClause = myDbHelper.STATE_PARENT_ID + " = ?";
        String[] whereArgs = { Long.toString(id)};
        Cursor cursor = db.query(myDbHelper.TABLE_ANIMATION_STATE, columns, whereClause,whereArgs,null ,null,myDbHelper.UID_STATE);

        ArrayList<AnimationState> states = new ArrayList<AnimationState>();
        while(cursor.moveToNext()){
            int cid = cursor.getInt(cursor.getColumnIndex(myDbHelper.UID_STATE));
            int state = cursor.getInt(cursor.getColumnIndex(myDbHelper.STATE));
            states.add(new AnimationState(state));
        }

        return states;
    }

    public  int delete(String uname)
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] whereArgs ={uname};
        int count =db.delete(myDbHelper.TABLE_ANIMATION ,myDbHelper.NAME+" = ?",whereArgs);
        return  count;
    }

    static class myDbHelper extends SQLiteOpenHelper
    {
        private static final String DATABASE_NAME = "LumenAnima";             // Database Name
        private static final String TABLE_ANIMATION = "Animation";            // Table Name
        private static final String TABLE_ANIMATION_STATE = "AnimationState"; // Table Name
        private static final int DATABASE_Version = 1;                        // Database Version
        private static final String UID_ANIMATION="_id";                      // Column I (Primary Key)
        private static final String NAME = "Name";                            // Column II
        private static final String PERIOD = "Period";                        // Column III
        private static final String UID_STATE="_id";                          // Column I (Primary Key)
        private static final String STATE_PARENT_ID="AnimationID";            // Column II (Foreign Key)
        private static final String STATE="State";                            // Column III

        private static final String CREATE_TABLE_ANIMATION = "CREATE TABLE "+TABLE_ANIMATION+
                " ("+UID_ANIMATION+" INTEGER PRIMARY KEY AUTOINCREMENT, "+NAME+" VARCHAR(255) ,"+ PERIOD+" int);";
        private static final String CREATE_TABLE_ANIMATION_STATE = "CREATE TABLE "+TABLE_ANIMATION_STATE+
                " ("+UID_STATE+" INTEGER PRIMARY KEY AUTOINCREMENT, "+STATE_PARENT_ID+" int ,"+ STATE +" int);";

        private static final String DROP_TABLE_ANIMATION ="DROP TABLE IF EXISTS "+TABLE_ANIMATION;

        private static final String DROP_TABLE_ANIMATION_STATE ="DROP TABLE IF EXISTS "+TABLE_ANIMATION_STATE;

        private Context context;

        public myDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_Version);
            this.context=context;
        }

        public void onCreate(SQLiteDatabase db) {

            try {
                db.execSQL(CREATE_TABLE_ANIMATION);
                db.execSQL(CREATE_TABLE_ANIMATION_STATE);
            } catch (Exception e) {
                Messager.message(context,""+e);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                Messager.message(context,"OnUpgrade");
                db.execSQL(DROP_TABLE_ANIMATION);
                db.execSQL(DROP_TABLE_ANIMATION_STATE);
                onCreate(db);
            }catch (Exception e) {
                Messager.message(context,""+e);
            }
        }
    }
}