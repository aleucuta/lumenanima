package com.lumenanima.controller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.widget.Toast;
import android.content.Context;

import java.io.Console;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;
import android.app.AlertDialog;
import android.widget.ArrayAdapter;

public class BluetoothController implements Serializable {

    private BluetoothAdapter bluetoothAdapter;
    private final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothSocket btSocket = null;
    private Context context;

    public static final int DEVICE_NO_BLUETOOTH = -1;
    public static final int TURN_BLUETOOTH_ON = -2;
    public static final int ERROR_ON_SOCKET = -3;
    public static final int SUCCESSFUL = 0;

    private static BluetoothController bluetoothControllerSingleton= null;

    public BluetoothController(Context context){
        int code = getDeviceBluetoothAdapter();
        this.context = context;
        if(code!=0){
            Messager.message(context,"Getting bluetooth adapter error:" + Integer.toString(code));
        }
    }

    public static BluetoothController getInstance(Context context){
        if(bluetoothControllerSingleton == null){
            bluetoothControllerSingleton = new BluetoothController(context);
        }
        return bluetoothControllerSingleton;
    }

    private int getDeviceBluetoothAdapter(){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            //Show a mensag. that thedevice has no bluetooth adapter
            //Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
            //finish apk
            //finish();
            return DEVICE_NO_BLUETOOTH;
        } else {
            if (bluetoothAdapter.isEnabled()) {
                return SUCCESSFUL;
            } else {
                //Ask to the user turn the bluetooth on
                //Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                //startActivityForResult(turnBTon, 1);
                return TURN_BLUETOOTH_ON;
            }
        }
        //return NOT_SUCCESSFUL;
    }

    public int connectToBluetoothDevice(String address){
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        try{
            btSocket = device.createInsecureRfcommSocketToServiceRecord(myUUID);
            btSocket.connect();
            if(btSocket != null){
                //writeToBluetoothSocket(0xDA11);
                return SUCCESSFUL;
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return ERROR_ON_SOCKET;
    }

    public BluetoothSocket getBtSocket() {
        return btSocket;
    }

    public void writeToBluetoothSocket(Byte message){
        if(btSocket!=null){
            try {
                btSocket.getOutputStream().write(message);
            }
            catch(IOException e){
                Messager.message(context,"Failed to write to socket");
                e.printStackTrace();
            }
        }
        else{
            Messager.message(context,"Bluetooth socket was null");
        }
    }
    public void writeToBluetoothSocket(int message){
        if(btSocket!=null){
            try {
                char octet = (char)(message & ((1 << 8) - 1));
                btSocket.getOutputStream().write(octet);

                octet = (char)((message & (((1 << 8) - 1)) << 8) >> 8);
                btSocket.getOutputStream().write(octet);
            }
            catch(IOException e){
                Messager.message(context,"Failed to write to socket");
                e.printStackTrace();
            }
        }
        else{
            Messager.message(context,"Bluetooth socket was null");
        }
    }
    public void writeToBluetoothSocket(Byte[] message){
        if(btSocket!=null){
            try {

                btSocket.getOutputStream().write(message[0]);
                btSocket.getOutputStream().write(message[1]);
            }
            catch(IOException e){
                Messager.message(context,"Failed to write to socket");
                e.printStackTrace();
            }
        }
        else{
            Messager.message(context,"Bluetooth socket was null");
        }
    }
}
