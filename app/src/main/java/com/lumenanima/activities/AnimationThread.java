package com.lumenanima.activities;

import com.lumenanima.controller.BluetoothController;
import com.lumenanima.model.Animation;
import com.lumenanima.model.AnimationState;

import java.util.ArrayList;

public class AnimationThread extends Thread{

    int period;
    ArrayList<AnimationState> states;
    BluetoothController btController;
    volatile boolean running = true;


    public AnimationThread(Animation animation, BluetoothController btController){
        this.period = animation.getPeriod();
        this.states = animation.getAnimationStates();
        this.btController = btController;
    }

    public void stopThread(){
        running = false;
    }

    public void run(){
        while(running){
            for(AnimationState state : states){
                if(!running)
                    return;
                btController.writeToBluetoothSocket(state.getBytesState());
                try{
                    Thread.sleep(period);
                }catch(InterruptedException e){

                }
            }
        }
    }
}
