package com.lumenanima.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.lumenanima.R;
import com.lumenanima.controller.BluetoothController;
import com.lumenanima.controller.Messager;
import com.lumenanima.controller.SQLiteAdapter;
import com.lumenanima.model.Animation;
import com.lumenanima.model.AnimationState;

import java.util.ArrayList;
import java.util.List;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;
import android.content.SharedPreferences;
import android.content.Context;

import java.util.Set;

public class MainActivity extends AppCompatActivity {


    private BluetoothSocket btSocket = null;
    private BluetoothController bluetoothController;
    private String bluetoothKey = null;
    private SharedPreferences preferences;
    private String prefName = "com.facultate.lumenanima";


    private void initItems(SQLiteAdapter adapter){

        //adapter.deleteAll();

        AnimationState danceAnimationState1 = new AnimationState(0b1010101010101010);
        AnimationState danceAnimationState2 = new AnimationState(0b0101010101010101);
        List<AnimationState> states = new ArrayList<AnimationState>();
        states.add(danceAnimationState1);
        states.add(danceAnimationState2);
        Animation danceAnimation = new Animation("dance",1000,states);

        long id = adapter.insertAnimation(danceAnimation);
        adapter.insertState(danceAnimationState1,id);
        adapter.insertState(danceAnimationState2,id);

        int stateSnakeSmall = 0b0000000000000011; //
        List<AnimationState> statesSnakeSmall = new ArrayList<AnimationState>();
        while(stateSnakeSmall <= 0b1100000000000000){
            statesSnakeSmall.add(new AnimationState(stateSnakeSmall));
            stateSnakeSmall<<=1;
        }
        Animation snakeSmallAnimation = new Animation("snakesmall",400,statesSnakeSmall);
        long id1 = adapter.insertAnimation(snakeSmallAnimation);
        for(AnimationState state : statesSnakeSmall){
            adapter.insertState(state,id1);
        }

        int stateSnakeBig = 0b0000000000011111; //
        List<AnimationState> statesSnakeBig = new ArrayList<AnimationState>();
        while(stateSnakeBig <= 0b1111100000000000){
            statesSnakeBig.add(new AnimationState(stateSnakeBig));
            stateSnakeBig<<=1;
        }
        Animation snakeBigAnimation   = new Animation("snakebig",400,statesSnakeBig);
        long id2 = adapter.insertAnimation(snakeBigAnimation);
        for(AnimationState state : statesSnakeBig){
            adapter.insertState(state,id2);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getSharedPreferences(prefName, Context.MODE_PRIVATE);

        SQLiteAdapter adapter= new SQLiteAdapter(this);
        bluetoothController = bluetoothController.getInstance(this);

        initItems(adapter);
    }


    public void customAnimationsButtonPressed(View view){
        Intent intent = new Intent(this, CustomAnimationsActivity.class);
        startActivity(intent);
    }

    public void presetAnimationsButtonPressed(View view){
        Intent intent = new Intent(this, PresetAnimationsActivity.class);
        startActivity(intent);
    }

    public void connectButtonPressed(View view){
        int status = bluetoothController.connectToBluetoothDevice(preferences.getString(bluetoothKey, null));
        String message = "Connecting to " +   preferences.getString(bluetoothKey, null);
        message += "\nStatus : " + Integer.toString(status);
        Messager.message(this,message);
        if (status == 0)
        {
            bluetoothController.writeToBluetoothSocket(0xDA11);
        }
    }

    public void chooseDeviceButtonPressed(View view){
        getAllDeviceAddress();
    }

    public void getAllDeviceAddress()
    {
        ArrayList deviceStrs = new ArrayList();
        final ArrayList devices = new ArrayList();

        BluetoothAdapter btAdapter=BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices=btAdapter.getBondedDevices();
        if (pairedDevices.size() > 0)
        {
            for (BluetoothDevice device : pairedDevices)
            {
                deviceStrs.add(device.getName() +    "\n"+device.getAddress());
                devices.add(device.getAddress());
            }
        }

        showDeviceSelecterDialog(deviceStrs, devices);

    }

    private void showDeviceSelecterDialog(ArrayList deviceStrs, final ArrayList devices){
        // show list
        final AlertDialog.Builder alertDialog =
                new AlertDialog.Builder(this);

        ArrayAdapter adapter =
                new ArrayAdapter(this,
                        android.R.layout.select_dialog_singlechoice,
                        deviceStrs.toArray(new String[deviceStrs.size()]));

        alertDialog.setSingleChoiceItems(adapter, -1,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        int position = ((AlertDialog) dialog)
                                .getListView()
                                .getCheckedItemPosition();
                        String deviceAddress = (String) devices.get(position);
                        SharedPreferences.Editor editor = getSharedPreferences(prefName, MODE_PRIVATE).edit();
                        editor.putString(bluetoothKey, deviceAddress).commit();

                    }
                });

        alertDialog.setTitle("Choose Bluetooth device");
        alertDialog.show();
    }

}
