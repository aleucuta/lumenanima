package com.lumenanima.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import com.lumenanima.R;
import com.lumenanima.controller.BluetoothController;
import com.lumenanima.controller.Messager;
import com.lumenanima.controller.RecyclerViewAdapter;
import com.lumenanima.controller.SQLiteAdapter;
import com.lumenanima.model.Animation;

import android.view.View;

import java.util.ArrayList;
import java.util.Set;

public class PresetAnimationsActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter recycleAdapter;
    private SQLiteAdapter dbAdapter;
    private BluetoothController btController;
    private AnimationThread animationThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println(savedInstanceState);
        setContentView(R.layout.preset_animations_view);

        dbAdapter = new SQLiteAdapter(this);
        Set<Animation> animations = dbAdapter.getAllAnimations();
        ArrayList<String> animationNames = new ArrayList<String>();
        for(Object animation : animations){
            animationNames.add(((Animation)animation).getName());
        }

        recyclerView = findViewById(R.id.recyclerViewStances);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recycleAdapter = new RecyclerViewAdapter(this, animationNames);
        recycleAdapter.setClickListener(this);
        recyclerView.setAdapter(recycleAdapter);

        dbAdapter = new SQLiteAdapter(this);
        btController = BluetoothController.getInstance(this);
    }


    @Override
    public void onItemClick(View view, int position) {
        if(btController.getBtSocket()==null){
            Messager.message(this,"Not connected to bluetooth device");
            return;
        }

        String name = recycleAdapter.getItem(position);

        Animation animation = dbAdapter.getAnimation(name);

        if(animationThread!=null){
            //animationThread.interrupt();
            animationThread.stopThread();
        }
        animationThread = new AnimationThread(animation,btController);
        animationThread.start();
    }

    public void backButtonPressed(View view){
        if(animationThread!=null){
            animationThread.stopThread();
            //animationThread.interrupt();

        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
