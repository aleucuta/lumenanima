package com.lumenanima.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lumenanima.R;
import com.lumenanima.controller.RecyclerViewAdapter;
import com.lumenanima.controller.Messager;
import com.lumenanima.controller.SQLiteAdapter;
import com.lumenanima.model.Animation;
import com.lumenanima.model.AnimationState;

import java.util.ArrayList;
import java.util.Set;

public class CustomAnimationsActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener{

    private RecyclerView recyclerView;
    private RecyclerViewAdapter recycleAdapter;
    private SQLiteAdapter dbAdapter;

    private CardView addAnimationCardView;
    private CardView addStanceCardView;
    private TextView animationStanceText;

    private ArrayList<String> states;
    private RecyclerView recyclerViewStances;
    private RecyclerViewAdapter adapterStances;
    private String selectedAnimationName;
    private RecyclerViewAdapter.ItemClickListener statesItemClickListener;
    private TextView animationPeriodText;
    private TextView animationNameText;
    private Button animationAddDone;
    private ArrayList<String> animationNames;
    private CustomAnimationsActivity me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println(savedInstanceState);
        setContentView(R.layout.custom_animations_view);
        dbAdapter = new SQLiteAdapter(this);
        Set<Animation> animations = dbAdapter.getAllAnimations();
        animationNames = new ArrayList<String>();
        for(Object animation : animations){
            animationNames.add(((Animation)animation).getName());
        }

        animationStanceText = findViewById(R.id.animationStanceText);
        addAnimationCardView = findViewById(R.id.cardView);
        addAnimationCardView.setVisibility(View.INVISIBLE);
        addStanceCardView = findViewById(R.id.cardViewStances);
        addStanceCardView.setVisibility(View.INVISIBLE);
        recyclerViewStances = findViewById(R.id.recyclerViewStances);
        recyclerView = findViewById(R.id.recyclerView);

        animationAddDone = findViewById(R.id.animationAddDone);
        animationPeriodText = findViewById(R.id.animationPeriodText);
        animationNameText = findViewById(R.id.animationNameText);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recycleAdapter = new RecyclerViewAdapter(this, animationNames);
        recycleAdapter.setClickListener(this);
        recyclerView.setAdapter(recycleAdapter);
        final Context context = this;

        statesItemClickListener = new RecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String animationName = selectedAnimationName;
                long animationId = dbAdapter.getAnimationId(animationName);
                dbAdapter.deleteStanceByPosition(animationId,position);

                states.remove(position);
                recyclerViewStances.setLayoutManager(new LinearLayoutManager(context));
                adapterStances = new RecyclerViewAdapter(context,states);
                adapterStances.setClickListener(this);
                recyclerViewStances.setAdapter(adapterStances);
            }
        };


        try{
            //setContentView(R.layout.activity_main);
            System.out.println("AM ajuns si aici");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(View view, int position) {

        String name = recycleAdapter.getItem(position);

        selectedAnimationName = name;

        Animation animation = dbAdapter.getAnimation(name);

        states = new ArrayList<String>();
        for (AnimationState animationState : animation.getAnimationStates()){
            states.add(animationState.getStringState());
        }

        addAnimationCardView.setVisibility(View.INVISIBLE);
        addStanceCardView.setVisibility(View.VISIBLE);

        recyclerViewStances.setLayoutManager(new LinearLayoutManager(this));
        adapterStances = new RecyclerViewAdapter(this, states);
        adapterStances.setClickListener(statesItemClickListener);
        recyclerViewStances.setAdapter(adapterStances);

    }

    public void backButtonPressed(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void addAnimationButtonPressed(View view){
        addStanceCardView.setVisibility(View.INVISIBLE);
        addAnimationCardView.setVisibility(View.VISIBLE);
        me = this;
        final Context context = this;
        animationAddDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int period=0;
                if(animationPeriodText.getText().length()>5){
                    Messager.message(context,"pls");
                    return;
                }
                for(char c : animationPeriodText.getText().toString().toCharArray()){
                    if(c<'0' || c>'9'){
                        Messager.message(context,"Period must contain only numbers " + c);
                        return;
                    }
                    period = period*10+(c-48);
                }

                String name = animationNameText.getText().toString();
                AnimationState blank = new AnimationState(0);
                ArrayList<AnimationState> mock = new ArrayList<AnimationState>(1);
                mock.add(blank);
                Animation animation = new Animation(name,period,mock);
                dbAdapter.insertAnimation(animation);

                //refresh animations list
                animationNames.add(name);
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                recycleAdapter = new RecyclerViewAdapter(context, animationNames);
                recycleAdapter.setClickListener(me);
                recyclerView.setAdapter(recycleAdapter);
            }
        });
    }

    public void addStateButtonPressed(View view){
        //validam state
        CharSequence state = animationStanceText.getText();
        int intState=0;
        if(state.length() != 16) {
            Messager.message(this, "State length must be 16");
            return;
        }
        String str="";
        for(char c : state.toString().toCharArray()){
            if(c!='0' && c!='1'){
                Messager.message(this,"State must have either 1 or 0 in composition");
                return;
            }
            str+=c;
            intState = intState*2 + (c-48);
        }
         //inseram state in db
        AnimationState animationState = new AnimationState(intState);
        Messager.message(this,selectedAnimationName);
        long animationId = dbAdapter.getAnimationId(selectedAnimationName);
        dbAdapter.insertState(animationState,animationId);

//        //updatam recycler view sa apara si state
        states.add(animationState.getStringState());
        recyclerViewStances.setLayoutManager(new LinearLayoutManager(this));
        adapterStances = new RecyclerViewAdapter(this,states);
        adapterStances.setClickListener(statesItemClickListener);
        recyclerViewStances.setAdapter(adapterStances);
    }
}
