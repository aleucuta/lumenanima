package com.lumenanima.model;

public class AnimationState {
    private boolean [] leds;

    public AnimationState(int state){
        leds = new boolean[16];
        Integer counter = -1;
        while(state != 0 || counter<15){
            if(state%2 == 1)
                leds[15-++counter] = true;
            else
                leds[15-++counter] = false;
            state/=2;
        }
    }

    public int getIntState(){
        int state = 0;
        for(boolean bit : this.leds){
            state = (state << 1) | (bit? 1 : 0);
        }
        return state;
    }

    public AnimationState(boolean[] state){
        leds = state;
    }

    public boolean[] getState(){
        return this.leds;
    }

    public String getStringState(){
        String string = "";
        for (boolean each : leds){
            string+=(each? 1 : 0);
        }
        return string;
    }
    public Byte[] getBytesState(){
        Byte state1=0,state2=0;
        int state = getIntState();
        state1 = ((Integer)(state&0x000000FF)).byteValue();
        state2 = ((Integer)((state&0x0000FF00) >> 8)).byteValue();
        return new Byte[]{state1,state2};
    }

    public void setState(boolean[] state){
        this.leds = state;
    }
}
