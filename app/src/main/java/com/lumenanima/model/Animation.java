package com.lumenanima.model;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Animation {
    private String name;
    private int period;
    private int numberOfStates;
    private List<AnimationState> animationStates;

    public Animation(String name, int period, List<AnimationState> animationStates){
        this.name = name;
        this.period = period;
        this.numberOfStates = animationStates.size();
        this.animationStates = animationStates;
    }

    public Animation(String name, int period, AnimationState[] animationStates){
        this.name = name;
        this.period = period;
        this.numberOfStates = animationStates.length;
        this.animationStates = new ArrayList<AnimationState>();
        this.animationStates = Arrays.asList(animationStates);
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getPeriod(){
        return this.period;
    }

    public void setPeriod(int period){
        this.period = period;
    }

    public int getNumberOfStates(){
        return this.numberOfStates;
    }

    public void setNumberOfStates(int nrOfStates){
        this.numberOfStates = nrOfStates;
    }

    public void addNewState(AnimationState animationState){
        this.animationStates.add(animationState);
        this.numberOfStates++;
    }

    public void removeKState(int index){
        this.animationStates.remove(index);
        this.numberOfStates--;
    }

    public void removeState(AnimationState animationState){
        this.animationStates.remove(animationState);
        this.numberOfStates--;
    }

    public ArrayList<AnimationState> getAnimationStates(){
        ArrayList<AnimationState> clone = new ArrayList<AnimationState>();
        clone.addAll(this.animationStates);
        return clone;
    }

    @Override
    public int hashCode() {
        int code = 0;
        for (char c : name.toCharArray()){
            code+=c%12012;
        }
        return code;
    }
}
